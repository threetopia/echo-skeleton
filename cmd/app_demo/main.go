package main

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/threetopia/echo-skeleton/internal/app/app_demo/handlers"
	"gitlab.com/threetopia/echo-skeleton/internal/app/app_demo/middlewares"
)

func main() {
	e := echo.New()
	m := middlewares.NewMiddleware(e)
	handlers.NewHandler(e, m)
	e.Logger.Fatal(e.Start(":1323"))
}
