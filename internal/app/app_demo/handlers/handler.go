package handlers

import (
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/threetopia/echo-skeleton/internal/app/app_demo/middlewares"
)

type Handler struct {
}

func NewHandler(e *echo.Echo, m *middlewares.Middleware) *Handler {
	handler := &Handler{}
	e.GET("/", handler.index, m.HandlerAudit)
	e.GET("/demo-page", handler.demoPage, m.HandlerAudit)

	return handler
}

func (h *Handler) index(c echo.Context) error {
	log.Println("This is index handler process")
	return c.String(http.StatusOK, "Hello World!")
}

func (h *Handler) demoPage(c echo.Context) error {
	log.Println("This is demoPage handler process")
	return c.String(http.StatusOK, "Demo Page")
}
