package middlewares

import (
	"github.com/labstack/echo/v4"
	"log"
)

type AuditMiddleware struct {
}

func newAuditMiddleware(e *echo.Echo) *AuditMiddleware {
	am := &AuditMiddleware{}
	e.Use(am.globalAudit)
	return am
}

func (am AuditMiddleware) globalAudit(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		log.Println("This before handler [GLOBAL SCOPE]")
		err := next(ctx)
		if err != nil {
			return err
		}
		log.Println("This after handler [GLOBAL SCOPE]")
		return nil
	}
}

func (am AuditMiddleware) HandlerAudit(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		log.Println("This before handler [HANDLER SCOPE]")
		err := next(ctx)
		if err != nil {
			return err
		}
		log.Println("This after handler [HANDLER SCOPE]")
		return nil
	}
}
