package middlewares

import "github.com/labstack/echo/v4"

type Middleware struct {
	*AuditMiddleware
	*ErrorMiddleware
}

func NewMiddleware(e *echo.Echo) *Middleware {
	return &Middleware{
		AuditMiddleware: newAuditMiddleware(e),
		ErrorMiddleware: newErrorMiddleware(e),
	}
}
