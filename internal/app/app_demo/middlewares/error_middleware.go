package middlewares

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"net/http"
)

type ErrorMiddleware struct {
}

func newErrorMiddleware(e *echo.Echo) *ErrorMiddleware {
	em := &ErrorMiddleware{}
	e.HTTPErrorHandler = em.CustomErrorHandler
	return em
}

func (em ErrorMiddleware) CustomErrorHandler(err error, ctx echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := err.(*echo.HTTPError); ok {
		code = he.Code
	}
	errorPage := fmt.Sprintf("%d.html", code)
	if err := ctx.File(errorPage); err != nil {
		ctx.Logger().Error(err)
	}
	err = ctx.JSON(http.StatusInternalServerError, echo.Map{
		"error": err.Error(),
	})
	if err != nil {
		ctx.Logger().Error(err)
	}
	ctx.Logger().Error(err)
}
